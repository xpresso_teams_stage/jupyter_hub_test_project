import os
import sys
import types
import pickle
import marshal
from xpresso.ai.core.utils.jupyter_experiment_utils import xpresso_save_plot
from xpresso.ai.core.commons.exceptions.xpr_exceptions import MountPathNoneType
## $xpr_param_component_name = dnn_fetch
## $xpr_param_component_type = pipeline_job
