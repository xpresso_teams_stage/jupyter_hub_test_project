import pandas as pd


## $xpr_param_component_name = dnn_preprocess
## $xpr_param_component_type = pipeline_job


def data_preprocessing():
    train_dataset = pd.read_csv('/data/training_dataset/train.csv')
    train_dataset['Initial'] = 0
    train_dataset['Initial'] = train_dataset.Name.str.extract('([A-Za-z]+)\.')
    # Checking the Initials with the Sex

    train_dataset['Initial'].replace(
        ['Mlle', 'Mme', 'Ms', 'Dr', 'Major', 'Lady', 'Countess', 'Jonkheer',
         'Col', 'Rev', 'Capt', 'Sir', 'Don', 'Miss', 'Miss', 'Miss', 'Mr',
         'Mr', 'Mrs', 'Mrs', 'Other', 'Other', 'Other', 'Mr', 'Mr', 'Mr'],
        inplace=True)
    train_dataset['Initial'] = 0
    train_dataset['Initial'] = train_dataset.Name.str.extract('([A-Za-z]+)\.')
    # Checking the Initials with the Sex
    # pd.crosstab(train.Initial, train.Sex).T.style.background_gradient(cmap='summer_r')

    train_dataset['Initial'].replace(
        ['Mlle', 'Mme', 'Ms', 'Dr', 'Major', 'Lady', 'Countess', 'Jonkheer',
         'Col', 'Rev', 'Capt', 'Sir', 'Don', 'Miss', 'Miss', 'Miss', 'Mr',
         'Mr', 'Mrs', 'Mrs', 'Other', 'Other', 'Other', 'Mr', 'Mr', 'Mr'],
        inplace=True)
    train_dataset['Embarked'].fillna('S', inplace=True)
    train_dataset['Age_band'] = 0
    train_dataset.loc[train_dataset['Age'] <= 16, 'Age_band'] = 0
    train_dataset.loc[(train_dataset['Age'] > 16) & (train_dataset['Age'] <= 32), 'Age_band'] = 1
    train_dataset.loc[(train_dataset['Age'] > 32) & (train_dataset['Age'] <= 48), 'Age_band'] = 2
    train_dataset.loc[(train_dataset['Age'] > 48) & (train_dataset['Age'] <= 64), 'Age_band'] = 3
    train_dataset.loc[train_dataset['Age'] > 64, 'Age_band'] = 4

    train_dataset['Family_Size'] = 0
    train_dataset['Family_Size'] = train_dataset['Parch'] + train_dataset['SibSp']  # family size
    train_dataset['Alone'] = 0
    train_dataset.loc[train_dataset.Family_Size == 0, 'Alone'] = 1  # Alone

    train_dataset['Sex'].replace(['male', 'female'], [0, 1], inplace=True)

    train_dataset['Embarked'].replace(['S', 'C', 'Q'], [0, 1, 2], inplace=True)

    train_dataset['Initial'].replace(['Mr', 'Mrs', 'Miss', 'Master', 'Other'],
                                     [0, 1, 2, 3, 4], inplace=True)
    train_dataset.drop(['Name', 'Age', 'Ticket', 'Fare', 'Cabin',
                        'PassengerId'], axis=1, inplace=True)
