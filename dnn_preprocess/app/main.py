"""
This is the implementation of data preparation for sklearn
"""

import sys
import logging
import pandas as pd

# Following two imports are required for Xpresso. Do not remove this.
from xpresso.ai.core.data.pipeline.abstract_pipeline_component import \
    AbstractPipelineComponent
from xpresso.ai.core.logging.xpr_log import XprLogger

__author__ = "Mrunalini Dhapodkar"

# To use the logger please provide the name and log level
#   - name is passed as the project name while generating the logs
#   - level can be DEBUG, INFO, WARNING, ERROR, CRITICAL
logger = XprLogger(name="dnn_preprocess",
                   level=logging.INFO)


class DnnPreprocess(AbstractPipelineComponent):
    """ Main class for any pipeline job. It is extended from AbstractPipelineComponent
    which allows xpresso platform to track and manage the pipeline.
    User will need to implement following method:
       -start: This is where the main functionality of the component is initiated.
          This method has a single parameter - the experiment run ID. This is automatically
          passed by xpresso.ai as the first argument when the component is run
        -completed: this is called when the main functionality of the component
          is complete, and results are to be stored if required.


    """

    def __init__(self, xpresso_run_name, parameters_filename,
                 parameters_commit_id):
        super().__init__(name="DnnPreprocess",
                         run_name=xpresso_run_name,
                         params_filename=parameters_filename,
                         params_commit_id=parameters_commit_id)
        # if you have specified parameters_filename or parameters_commit_id,
        # the run parameters can be accessed from self.run_parameters
        """ Initialize all the required constants and data here """

    def start(self, xpresso_run_name):
        """
        This is the start method, which does the actual data preparation.
        As you can see, it does the following:
          - Calls the superclass start method - this notifies the Controller that
              the component has started processing (details such as the start
              time, etc. are appropriately stored by the Controller)
          - Main data processing or training codebase.
          - It calls the completed method when it is done

        Args:
            xpresso_run_name: xpresso run name which is used by base class to
            identify the current run. It must be passed. While running as
            pipeline, xpresso automatically adds it.

        """
        try:
            super().start(xpresso_run_name=xpresso_run_name)
            # === Your start code base goes here ===

        except Exception:
            import traceback
            traceback.print_exc()
            self.completed(success=False)
        self.completed(success=True)

    def data_preprocessing(self):
        train_dataset = pd.read_csv('/data/training_dataset/train.csv')
        train_dataset['Initial'] = 0
        train_dataset['Initial'] = train_dataset.Name.str.extract('([A-Za-z]+)\.')
        # Checking the Initials with the Sex

        train_dataset['Initial'].replace(
            ['Mlle', 'Mme', 'Ms', 'Dr', 'Major', 'Lady', 'Countess', 'Jonkheer',
             'Col', 'Rev', 'Capt', 'Sir', 'Don', 'Miss', 'Miss', 'Miss', 'Mr',
             'Mr', 'Mrs', 'Mrs', 'Other', 'Other', 'Other', 'Mr', 'Mr', 'Mr'],
            inplace=True)
        train_dataset['Initial'] = 0
        train_dataset['Initial'] = train_dataset.Name.str.extract('([A-Za-z]+)\.')
        # Checking the Initials with the Sex
        # pd.crosstab(train.Initial, train.Sex).T.style.background_gradient(cmap='summer_r')

        train_dataset['Initial'].replace(
            ['Mlle', 'Mme', 'Ms', 'Dr', 'Major', 'Lady', 'Countess', 'Jonkheer',
             'Col', 'Rev', 'Capt', 'Sir', 'Don', 'Miss', 'Miss', 'Miss', 'Mr',
             'Mr', 'Mrs', 'Mrs', 'Other', 'Other', 'Other', 'Mr', 'Mr', 'Mr'],
            inplace=True)
        train_dataset['Embarked'].fillna('S', inplace=True)
        train_dataset['Age_band'] = 0
        train_dataset.loc[train_dataset['Age'] <= 16, 'Age_band'] = 0
        train_dataset.loc[(train_dataset['Age'] > 16) & (train_dataset['Age'] <= 32), 'Age_band'] = 1
        train_dataset.loc[(train_dataset['Age'] > 32) & (train_dataset['Age'] <= 48), 'Age_band'] = 2
        train_dataset.loc[(train_dataset['Age'] > 48) & (train_dataset['Age'] <= 64), 'Age_band'] = 3
        train_dataset.loc[train_dataset['Age'] > 64, 'Age_band'] = 4

        train_dataset['Family_Size'] = 0
        train_dataset['Family_Size'] = train_dataset['Parch'] + train_dataset['SibSp']  # family size
        train_dataset['Alone'] = 0
        train_dataset.loc[train_dataset.Family_Size == 0, 'Alone'] = 1  # Alone

        train_dataset['Sex'].replace(['male', 'female'], [0, 1], inplace=True)

        train_dataset['Embarked'].replace(['S', 'C', 'Q'], [0, 1, 2], inplace=True)

        train_dataset['Initial'].replace(['Mr', 'Mrs', 'Miss', 'Master', 'Other'],
                                         [0, 1, 2, 3, 4], inplace=True)
        train_dataset.drop(['Name', 'Age', 'Ticket', 'Fare', 'Cabin',
                            'PassengerId'], axis=1, inplace=True)

    def send_metrics(self):
        """ It is called to report intermediate status. It reports status and
        metrics back to the xpresso.ai controller through the report_status
        method of the superclass. The Controller stores any metrics reported in
        a database, and makes these available for comparison. It needs the
        following format:
        - status:
           - status - <single word description>
        - metric:
           - Key-Value - Of the metrics that needs to be tracked and visualized
                         realtime. This could be data size, accuracy, loss etc.
        """
        try:
            report_status = {
                "status": {"status": "data_preparation"},
                "metric": {"metric_key": 1}
            }
            self.report_status(status=report_status)
        except Exception:
            import traceback
            traceback.print_exc()

    def completed(self, push_exp=True, success=True):
        """
        This is the completed method. It stores the output data files on the
        file system, and then calls the superclass completed method, which notes
        the fact that the component has completed processing, along with the end time.

        User must need to call super completed method at the end of the method
        Args:
            push_exp: Whether to push the data present in the output folder
               to the versioning system. This is required once training is
               completed and model needs to be versioned
            success: Use to handle failure cases

        """
        self.data_preprocessing()
        try:
            super().completed(push_exp=push_exp, success=success)
        except Exception:
            import traceback
            traceback.print_exc()
            sys.exit(1)

    def terminate(self):
        """
        This is used to shutdown the current pipeline execution. All the
        component in the pipeline will be terminated. Once terminated, the
        current pipeline execution cannot be restarted later.

        """
        # === Your start code base goes here ===
        try:
            super().terminate()
        except Exception:
            import traceback
            traceback.print_exc()
            sys.exit(1)

    def pause(self, push_exp=True):
        """
        Pause method is used to pause the execution of the job so that it
        can be restarted at some later point. User should implement this function
        to save the state of the current execution. This state will be used
        on restart.
        Args:
            push_exp: Whether to push the data present in the output folder
               to the versioning system. This is required once training is
               completed and model needs to be versioned
        """
        # === Your start code base goes here ===
        try:
            super().pause()
        except Exception:
            import traceback
            traceback.print_exc()
            sys.exit(1)

    def restart(self):
        """
        Restart method is used to start any previously paused experiment. It
        starts the experiment from the same state which was stored when pause
        experiment was called. This should implement the logic to
        reload the state of the previous run.
        """
        # === Your start code base goes here ===
        try:
            super().restart()
        except Exception:
            import traceback
            traceback.print_exc()
            sys.exit(1)


if __name__ == "__main__":
    # To run locally. Use following command:
    # XPRESSO_PACKAGE_PATH=$PWD/../xpresso_ai enable_local_execution=true python app/main.py

    run_name = ""
    params_filename = None
    params_commit_id = None
    if len(sys.argv) >= 2:
        run_name = sys.argv[1]
    if len(sys.argv) >= 4:
        params_filename = sys.argv[2] if sys.argv[2] != "None" else None
        params_commit_id = sys.argv[3] if sys.argv[3] != "None" else None

    pipeline_job = DnnPreprocess(run_name, params_filename, params_commit_id)
    pipeline_job.start(xpresso_run_name=run_name)
