## Default Makefile generated for the  project : Xpresso.AI
## Testing something something


branch_name := `git branch | grep \* | cut -d ' ' -f2`
branch_name_str := `git branch | grep \* | cut -d ' ' -f2 | tr -d '/\ '`

build_folder := ${CURDIR}/xprbuild
docker_build_folder := ${build_folder}/docker
k8_build_folder := ${build_folder}/docker
linux_build_folder := ${build_folder}/system/linux
windows_build_folder := ${build_folder}/system/windows

DOCKER_IMAGE_NAME := xpresso_ai
PROJECT_VERSION := $(shell cat VERSION)
export PROJECT_VERSION
export ROOT_FOLDER := ${CURDIR}
export PROJECT_NAME := xpresso.ai

default: debug

# Clean the projects
clean:
	@echo "--------------- Clean Started ----------------"
	rm -rf dist/
	rm -rf build/
	@echo "--------------- Clean Completed ----------------"

clobber: clean
	@echo "--------------- Clobber Started ----------------"
	find . -name '*.pyc' -exec rm --force {} +
	find . -name '*.pyo' -exec rm --force {} +
	find . -name '*~' -exec rm --force {} +
	@echo "--------------- Clobber Completed ----------------"

# Much needed git commands
checkout:
	git pull

update: clean checkout

push: clean
	git squash
	git push origin ${branch_name}

patch: clean
	@if [ -z "${ARGS}" ];then\
		echo "\nError: Pass remote branch for creating patch.\n--usage: make patch master";\
	else\
		echo  "Creating patch for following commits";\
		git log --pretty=oneline -1;\
		git format-patch ${ARGS} --stdout > ${branch_name_str}.patch;\
	fi;

# Perform test
lint:
	@echo "--------------- Quality Check Started ----------------"
	@echo "Running pylint"
	pylint xpresso
	@echo "--------------- Quality Check Completed ----------------"

unittest:
	@echo "Performing Unit Test"
	/bin/bash ${docker_build_folder}/test.sh ${DOCKER_IMAGE_NAME} ${TAG}

apitest:
	@echo "Performing API Testing"
	/bin/bash  ${docker_build_folder}/test.sh ${DOCKER_IMAGE_NAME} ${TAG}

systemtest:
	@echo "Performing System Testing"

test-all: unittest apitest systemtest

# Build
prepare:
	@echo "Installing Dependencies"
	/bin/bash ${linux_build_folder}/pre-build.sh

debug: clean
	@echo "Building docker version: ${PROJECT_VERSION}"
	/bin/bash ${docker_build_folder}/build.sh ${DOCKER_IMAGE_NAME} ${TAG}

release: clobber dependency
	@echo "Create release docker image"
	/bin/bash ${docker_build_folder}/build.sh release

module: clean
	@echo "Creates modules which can be distributed across using pypi repository"

all: debug release

build: clobber
	@echo "Building docker version: ${PROJECT_VERSION}"
	/bin/bash ${docker_build_folder}/build.sh ${DOCKER_IMAGE_NAME} ${PROJECT_VERSION} ${SUFFIX}

dist: clobber
	@echo "Generate distribution"

# Train
train:
	@echo "Training the datacode"

# Deployment
dockerpush:
	@echo "Tagging the docker image and pushing"
	docker login dockerregistry.xpresso.ai -u admin -p Abz00ba@123
	docker push ${DOCKER_IMAGE_NAME}:${TAG}

run-local:
	@echo "Running deploy local application"
	/bin/bash ${linux_build_folder}/run.sh

run-docker:
	@echo "Running deploy local application"
	/bin/bash ${docker_build_folder}/run.sh

run: run-local

# Playground
build-local-env:
	docker build \
	-t dockerregistry.xpresso.ai/library/xpresso_local:${PROJECT_VERSION} \
	-f xprbuild/docker/Dockerfile_local .

local-env:
	/bin/bash scripts/python/local_development/start-env.sh --command start

local-env-push:
	/bin/bash scripts/python/local_development/start-env.sh --command push

install-server:
	@echo "Installing using systemctl service"
	chmod +x ${linux_build_folder}/build.sh

	/bin/bash ${linux_build_folder}/build.sh
	touch xpresso-controller.service
	chmod +x ${linux_build_folder}/run.sh
	cp config/xpresso-controller.service /etc/systemd/system/xpresso-controller.service
	systemctl daemon-reload
	systemctl enable xpresso-controller.service
	systemctl start xpresso-controller
	systemctl status xpresso-controller

install:
	@echo "Installing xpresso.ai library"
	/bin/bash ${linux_build_folder}/install_alluxio.sh
	pip install -e .
	@echo "Installation Completed"

install-client:
	@echo "Installing Xpresso client "
	chmod +x ${linux_build_folder}/build_client.sh
	/bin/bash ${linux_build_folder}/build_client.sh

update-client:
	@echo "Updating xprctl client"
	pip install -e .
	@echo "xprctl client updated to ${PROJECT_VERSION}"


dist-client: clean
	@echo "Creating a distributions for xpresso client..."
	chmod +x ${linux_build_folder}/generate_dist_client.sh
	/bin/bash ${linux_build_folder}/generate_dist_client.sh

# utils
doc:
	@echo "This should generate the docs"

version:
	@echo "This should print the version"
	echo "${PROJECT_VERSION}"

.PHONY:
	clean all debug debug-local release deploy deploy-local run run-local
