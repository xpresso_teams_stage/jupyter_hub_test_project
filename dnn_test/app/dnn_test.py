import os
import sys
import types
import pickle
import marshal
import argparse
import warnings
import pandas as pd
import seaborn as sns
import matplotlib.pyplot as plt
from sklearn import metrics  # accuracy measure
from sklearn import svm  # support vector Machine
from sklearn.neighbors import KNeighborsClassifier  # KNN
from sklearn.naive_bayes import GaussianNB  # Naive bayes
from sklearn.tree import DecisionTreeClassifier  # Decision Tree
from sklearn.model_selection import cross_val_predict  # prediction
from sklearn.ensemble import RandomForestClassifier  # Random Forest
from sklearn.metrics import confusion_matrix  # for confusion matrix
from sklearn.model_selection import cross_val_score  # score evaluation
from sklearn.model_selection import KFold  # for K-fold cross validation
from sklearn.linear_model import LogisticRegression  # logistic regression
from sklearn.model_selection import train_test_split  # training and testing data split
from xpresso.ai.core.utils.jupyter_experiment_utils import xpresso_save_plot
from xpresso.ai.core.commons.exceptions.xpr_exceptions import MountPathNoneType
## $xpr_param_component_name = dnn_test
## $xpr_param_component_type = pipeline_job
def test_model(self, args, test_X, test_Y, model, X, Y):

    prediction = model.predict(test_X)
    return prediction
