import pandas as pd
from sklearn.neighbors import KNeighborsClassifier  # KNN
from sklearn.model_selection import train_test_split  # training and testing data split
from xpresso.ai.core.utils.jupyter_experiment_utils import xpresso_save_plot
from xpresso.ai.core.commons.exceptions.xpr_exceptions import MountPathNoneType


## $xpr_param_component_name = dnn_train
## $xpr_param_component_type = pipeline_job


def train_model():
    train_dataset = pd.read_csv('/data/training_dataset/train.csv')
    train_data, test_data = train_test_split(train_dataset, test_size=0.3,
                                             random_state=0, stratify=train_dataset['Survived'])
    train_X = train_data[train_data.columns[1:]]
    train_Y = train_data[train_data.columns[:1]]
    test_X = test_data[test_data.columns[1:]]
    test_Y = test_data[test_data.columns[:1]]
    X = train_dataset[train_dataset.columns[1:]]
    Y = train_dataset['Survived']
    model_KNN = KNeighborsClassifier()
    model_KNN.fit(train_X, train_Y)
    return test_X, test_Y, model_KNN, X, Y
